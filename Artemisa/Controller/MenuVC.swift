//
//  MenuVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 18/02/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit
class MenuCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var view_icon: UIView!
    @IBOutlet weak var img_icon: UIImageView!
    @IBOutlet weak var lbl_menu: UILabel!
    
}
class MenuVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    @IBOutlet weak var cv_menu: UICollectionView!
    
    var menu_options = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cv_menu.backgroundColor = UIColor.white
        let opt1 = NSMutableDictionary()
        opt1.setValue("Notificaciones", forKey: "name")
        opt1.setValue("notif.png", forKey: "image")
        menu_options.add(opt1)
        
//        let opt2 = NSMutableDictionary()
//        opt2.setValue("Remisiones", forKey: "name")
//        opt2.setValue("rem.png", forKey: "image")
//        menu_options.add(opt2)
        
        let opt3 = NSMutableDictionary()
        opt3.setValue("Órdenes de compra", forKey: "name")
        opt3.setValue("oc.png", forKey: "image")
        menu_options.add(opt3)
        
        let opt4 = NSMutableDictionary()
        opt4.setValue("Reportes", forKey: "name")
        opt4.setValue("reports.png", forKey: "image")
        menu_options.add(opt4)
        
        // Do any additional setup after loading the view.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menu_options.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_menu", for: indexPath) as! MenuCollectionViewCell
        cell.backgroundColor = UIColor.white
        let menu = menu_options[indexPath.row] as! NSDictionary
        cell.lbl_menu.text = menu.object(forKey: "name") as? String
        let name_img = menu.object(forKey: "image") as? String
        cell.img_icon.image = UIImage.init(named: name_img ?? "")
      
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(indexPath.row == 2)
        {
            performSegue(withIdentifier: "segue_reports", sender: nil)
        }
    }
    
    @IBAction func close_session(_ sender: Any) {
        
        self.navigationController!.dismiss(animated: true, completion:nil);
    }
    
    //MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "segue_reports" {

        }
    }
    

}
