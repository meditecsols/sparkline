//
//  ForgotPasswordVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 22/06/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit
import SwiftSpinner

class ForgotPasswordVC: UIViewController {
    let invokeServ = InvokeService()
    let alertmanager = AlertManager()

    @IBOutlet weak var txt_email: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        txt_email.attributedPlaceholder = NSAttributedString(string:"Correo electrónico", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        //Looks for single or multiple taps.
              let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

              //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
              //tap.cancelsTouchesInView = false

              view.addGestureRecognizer(tap)
              
              observeKeyboardNotifications()
        // Do any additional setup after loading the view.
    }
    fileprivate func observeKeyboardNotifications()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShow), name:UIResponder.keyboardWillShowNotification , object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name:UIResponder.keyboardWillHideNotification , object: nil)
        
    }
    @objc func keyboardHide()
    {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.frame = CGRect(x: 0, y:0, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
        
    }

    @objc func keyboardShow()
    {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.frame = CGRect(x: 0, y: -150, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
    
    }
    
    @IBAction func back_action(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func recover_action(_ sender: Any) {
        
        if(txt_email.text?.count == 0)
        {
            alertmanager.alert(title: "Apolo.bi", mess: "Ingresa un correo", viewcontroller: self)
            return
        }
        else
        {
            if !isValidEmail(emailStr: txt_email.text!)
            {
                alertmanager.alert(title: "Apolo.bi",mess: "Ingresa un correo valido" , viewcontroller: self);
                return
            }
        }
        let dic: NSMutableDictionary? = ["email" : txt_email.text!
        ]
        
        SwiftSpinner.show("Recuperando...")
        invokeServ.requestForRecoverDataWith(dic as! [NSMutableDictionary : NSMutableDictionary]) { (response, error, code) in
            if(code == 201)
            {
                DispatchQueue.main.async
                {
                    SwiftSpinner.hide()
             
                   let alertController = UIAlertController(title: "Apolo.bi", message: "Revisa tu correo electrónico. Te hemos enviado las instrucciones para restaurar tu contraseña", preferredStyle: .alert)
                    
               
                   let okAction = UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default) {
                       UIAlertAction in
                        self.dismiss(animated: true, completion: nil)
                        let mailURL = URL(string: "message://")!
                        if UIApplication.shared.canOpenURL(mailURL) {
                            UIApplication.shared.open(mailURL, options: [:], completionHandler: nil)
                            
                         }
                      
                   }

                  
                   alertController.addAction(okAction)

                   
                   self.present(alertController, animated: true, completion: nil)
                }
                
            }
            else if(code == 404)
            {
                DispatchQueue.main.async
                {
                    SwiftSpinner.hide()
                    self.alertmanager.alert(title: "Apolo.bi",mess: "Este correo no esta registrado, favor de revisar e intentar de nuevo" , viewcontroller: self);
                }
            }
        }
    }
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
