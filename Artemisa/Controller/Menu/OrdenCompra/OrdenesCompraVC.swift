//
//  OrdenesCompraVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 16/04/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit
import SwiftSpinner

class OrdenesTViewCell: UITableViewCell
{
      @IBOutlet weak var lbl_company: UILabel!
      @IBOutlet weak var lbl_unit_business: UILabel!
      @IBOutlet weak var lbl_proyect: UILabel!
      @IBOutlet weak var lbl_suppliers: UILabel!
      @IBOutlet weak var lbl_reference: UILabel!
      @IBOutlet weak var lbl_department: UILabel!
      @IBOutlet weak var lbl_cost: UILabel!
}


class OrdenesCompraVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var lbl_title: UILabel!
  
    @IBOutlet weak var tv_ordenes: UITableView!
    
    let invokeServ = InvokeService()
    let alertmanager = AlertManager()
    
    @IBOutlet weak var lbl_message: UILabel!
    var ordenes_array = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(true)
            
            self.tv_ordenes.backgroundView?.backgroundColor = UIColor.white
            self.tv_ordenes.backgroundColor = UIColor.white
        lbl_message.isHidden = true
        
            let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyKey)
            if(saved_company != nil)
            {
                let comp_shortname = saved_company!["short_name"] as! String
                let engrane_name = saved_company!["engrane_name"] as! String
                lbl_title.text = comp_shortname
                //getOrdenes(company_id: comp_id)
                //self.tabBarController!.selectedIndex = 1
            }
            else
            {
                alertmanager.alert(title: "Apolo.bi",mess: "Selecciona una empresa" , viewcontroller: self);
                self.tabBarController!.selectedIndex = 2
            }
          
    }
    
    func getOrdenes(company_id : String) {
        ordenes_array = NSMutableArray()
         SwiftSpinner.show("Cargando...")
        invokeServ.requestOrdennesDatabyCompanyWith(company_id: company_id)
        { (response, error) in
            
            if error == nil
            {
                
                    let cxc_array = response!["bi_po_unauthorized_by_company_and_supplier"] as! NSArray
                   
                    self.ordenes_array = NSMutableArray(array:cxc_array)
                    
                
                    DispatchQueue.main.async
                    {
                        if(self.ordenes_array.count > 0)
                        {
                            self.lbl_message.isHidden = true
                            self.tv_ordenes.isHidden = false
                        }
                        else
                        {
                            self.tv_ordenes.isHidden = true
                            self.lbl_message.isHidden = false
                        }
                        SwiftSpinner.hide()
                        self.tv_ordenes.reloadData()
                    }

                
            }
            
        }
                
                        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ordenes_array.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 158
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = self.contextualDeleteAction(forRowAtIndexPath: indexPath)
        let flagAction = self.contextualToggleFlagAction(forRowAtIndexPath: indexPath)
        let swipeConfig = UISwipeActionsConfiguration(actions: [deleteAction, flagAction])
        return swipeConfig
    }
    func contextualToggleFlagAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        // 1
  
        let action = UIContextualAction(style: .normal,
                                        title: "Aceptar") { (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in
           
        }
        // 7
        //action.image = UIImage(named: "flag")
        action.backgroundColor = UIColor.orange
        return action
    }
    
    func contextualDeleteAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
          // 1
    
          let action = UIContextualAction(style: .normal,
                                          title: "Rechazar") { (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in
             
          }
          // 7
          //action.image = UIImage(named: "flag")
          action.backgroundColor = UIColor.lightGray
          return action
      }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ordenes_cell", for: indexPath) as! OrdenesTViewCell
                    cell.backgroundColor = UIColor.white
                    let order =  ordenes_array[indexPath.row] as! NSDictionary
                    cell.lbl_company.text = "Empresa/Cuenta: " + (order["company_shortname"] as! String)
                    cell.lbl_unit_business.text = "Unidad de negocio: " + (order["businessunit_name"] as! String)
                    cell.lbl_proyect.text = "Proyecto: " + (order["businessunit_project_name"] as! String)
                    cell.lbl_suppliers.text = "Proveedor: " + (order["supplier_shortname"] as! String)
                    cell.lbl_reference.text = "Ref./Folio: " + (order["reference"] as! String) + "/" + (order["consecutive"] as! String)
                    cell.lbl_department.text = "Departamento: " + (order["costcenter_name"] as! String)
                    let purchaseorder_amount = order["purchaseorder_amount"] as! String
                    let purchaseorder_amount_double = Double(purchaseorder_amount)
                    cell.lbl_cost.text = convertDoubleToCurrency(amount: purchaseorder_amount_double!)
                    
               
                    
                    return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
