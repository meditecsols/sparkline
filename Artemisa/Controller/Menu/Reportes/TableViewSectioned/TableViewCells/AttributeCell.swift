//
//  AttributeCell.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 13/04/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit

class AttributeCell: UITableViewCell {
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_amount: UILabel!
    
    var item: NSDictionary?  {
        didSet {
            lbl_name?.text = ""
            lbl_amount?.text = ""
        }
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }

}
