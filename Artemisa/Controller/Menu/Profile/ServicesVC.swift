//
//  ServicesVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 25/08/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit
import SwiftSpinner

class ServicesCell: UITableViewCell
{
    @IBOutlet weak var lbl_security: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    
    @IBOutlet weak var lbl_authorized: UILabel!
    @IBOutlet weak var lbl_text_authorized: UILabel!
}

class ServicesVC: UIViewController,UITableViewDelegate,UITableViewDataSource  {
   
    @IBOutlet weak var tv_services: UITableView!
    let invokeServ = InvokeService()
    let alertmanager = AlertManager()
    
    var services_array = NSArray()
    var services_user_array = NSArray()
    
    var id_user = Int()
    var director = NSDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()
        tv_services.isHidden = true
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(true)
           
           self.tv_services.backgroundView?.backgroundColor = UIColor.white
           self.tv_services.backgroundColor = UIColor.white
           getservices()
    }
    
    func getservices() {
        services_array = NSMutableArray()
        SwiftSpinner.show("Cargando...")
        invokeServ.requestServicesDataWith{ (response, error) in
                        
                        if response != nil
                        {
                            self.services_array = response!
                            
                            DispatchQueue.main.async
                            {
//                                if(self.services_array.count > 0)
//                                {
//                                    self.tv_services.isHidden = false
//                                }
//                                SwiftSpinner.hide()
//                                self.tv_services.reloadData()
                                
                                self.getservicesbyuser()
                            }
                            
                        }
                        else
                        {
                            DispatchQueue.main.async
                            {
                                SwiftSpinner.hide()
                               
                            }
                            
                        }
                        
                        
                    }
        
    }
    func getservicesbyuser() {
        services_user_array = NSMutableArray()
        SwiftSpinner.show("Cargando...")
        invokeServ.requestPermissionDataWith(id_worker: String(id_user)){ (response, error) in
                        
                        if response != nil
                        {
                            self.services_user_array = response!
                            
                            DispatchQueue.main.async
                            {
                                if(self.services_array.count > 0)
                                {
                                    self.tv_services.isHidden = false
                                }
                                SwiftSpinner.hide()
                                self.tv_services.reloadData()
                            }
                            
                        }
                        else
                        {
                            DispatchQueue.main.async
                            {
                                SwiftSpinner.hide()
                               
                            }
                            
                        }
                        
                        
                    }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return services_array.count
        
       }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "service_cell", for: indexPath) as! ServicesCell
        
        cell.backgroundColor = UIColor.white
        cell.contentView.backgroundColor = UIColor.white
        
        let service = services_array[indexPath.row] as! NSDictionary
        let security_type = (service.object(forKey: "security_type") as! String)
        
      
        switch security_type {
        case "high":
            cell.lbl_security.text = "Alto"
            break
        case "medium":
            cell.lbl_security.text = "Medio"
            break
        case "low":
            cell.lbl_security.text = "Bajo"
            break
        default: break
            
        }
        
        let service_name = service.object(forKey: "name") as! String
        cell.lbl_name.text = (service_name)
        cell.lbl_description.text = (service.object(forKey: "description") as! String)
        cell.accessoryType = .none
        cell.lbl_text_authorized.isHidden = true
        cell.lbl_authorized.isHidden = true
        
        for item in services_user_array {
            
            let serv = item as! NSDictionary
            let service_dic = serv.object(forKey: "service") as! NSDictionary
            let service_name_user = service_dic.object(forKey: "name") as! String
            
            if(service_name.elementsEqual(service_name_user))
            {
                cell.lbl_text_authorized.isHidden = false
                cell.lbl_authorized.isHidden = false
                
                let authorized_by = serv.object(forKey: "authorized_by") as! NSDictionary
                let first_name = authorized_by.object(forKey: "first_name") as! String
                let last_name = authorized_by.object(forKey: "last_name") as! String
                cell.lbl_authorized.text = first_name + " " + last_name
                cell.accessoryType = .checkmark
            }

            
        }
        
        return cell
           
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        let service = services_array[indexPath.row] as! NSDictionary
        let service_id = service.object(forKey: "id") as! Int
        let director_id = self.director.object(forKey: "id") as! Int
        let service_name = service.object(forKey: "name") as! String
        
        var permission_id = ""
        for item in services_user_array {
                   
                   let serv = item as! NSDictionary
                   let service_dic = serv.object(forKey: "service") as! NSDictionary
                   let service_name_user = service_dic.object(forKey: "name") as! String
                   
                   if(service_name.elementsEqual(service_name_user))
                   {
                        let id = serv.object(forKey: "id") as! Int
                        permission_id = String(id)
                   }

                   
         }
        
        if(cell!.accessoryType == .checkmark)
        {
           
            let refreshAlert = UIAlertController(title: "Apolo", message: "¿Deseas quitar el permiso a este usuario?", preferredStyle: UIAlertController.Style.alert)

            refreshAlert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action: UIAlertAction!) in
                
                let dic = NSMutableDictionary()
                dic.setValue(false, forKey: "active")
                
                self.invokeServ.requestRemovePermissionDataWith(id_permission: permission_id,params: dic) { (response,error, code) in
                    if(code == 200)
                    {
                        
                         DispatchQueue.main.async {
                             self.dismiss(animated: true, completion: nil)
                         }
                    }
                }
             
              }))

            refreshAlert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: { (action: UIAlertAction!) in
           
              }))

            present(refreshAlert, animated: true, completion: nil)
        }
        else
        {
            let refreshAlert = UIAlertController(title: "Apolo", message: "¿Estás seguro de dar permiso a este usuario?", preferredStyle: UIAlertController.Style.alert)

             refreshAlert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action: UIAlertAction!) in
                
                let dic = NSMutableDictionary()
                dic.setValue(service_id, forKey: "service")
                dic.setValue(director_id , forKey: "authorized_by")
                dic.setValue(self.id_user , forKey: "worker")
                 
                self.invokeServ.requestaddPermissionDataWith(params: dic) { (response, error, code) in
                    if(code == 201)
                    {
                        DispatchQueue.main.async {
                            self.dismiss(animated: true, completion: nil)
                        }
                        
                    }
                    
                }
              
               }))

             refreshAlert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: { (action: UIAlertAction!) in
            
               }))

             present(refreshAlert, animated: true, completion: nil)
        }
        
    }

    @IBAction func back_action(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
