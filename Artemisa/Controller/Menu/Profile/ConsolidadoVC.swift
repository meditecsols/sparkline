//
//  ConsolidadoVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 20/05/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit
import SwiftSpinner

class ConsolidadoCell : UITableViewCell
{
    @IBOutlet weak var lbl_name: UILabel!
    
    @IBOutlet weak var lbl_suppliers: UILabel!
    @IBOutlet weak var lbl_clients: UILabel!
    @IBOutlet weak var lbl_bank: UILabel!
}

class ConsolidadoVC: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var tv_consolidado: UITableView!
    @IBOutlet weak var btnfilter: UIButton!
    
    @IBOutlet weak var view_totales: UIView!
    @IBOutlet weak var lbl_bancos_total: UILabel!
    @IBOutlet weak var lbl_proveedores_total: UILabel!
    @IBOutlet weak var lbl_clientes_total: UILabel!
    
    let invokeServ = InvokeService()
    let alertmanager = AlertManager()
    
    var cons_array = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tv_consolidado.refreshControl = UIRefreshControl()
        tv_consolidado.refreshControl?.addTarget(self, action:
                                             #selector(handleRefreshControl),
                                             for: .valueChanged)
        self.view_totales.backgroundColor = UIColor(red: 153/255.0, green: 135/255.0, blue: 157/255.0, alpha: 1.0).withAlphaComponent(0.1)
        self.btnfilter.backgroundColor = UIColor(red: 252/255.0, green: 125/255.0, blue: 88/255.0, alpha: 1.0).withAlphaComponent(0.1)
        self.tv_consolidado.backgroundColor = UIColor.white
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        let consolidado = GlobalMembers.preferences.array(forKey: GlobalMembers.consolidadoKey) ?? [Any]()
        if(consolidado.count>0)
        {
            self.cons_array = consolidado as NSArray
            self.filldata()
        }
        else
        {
            self.getConsolidado()
        }
        //getConsolidado()
    }
    @objc func handleRefreshControl() {
    
       DispatchQueue.main.async {
          SwiftSpinner.show("Cargando...")
          self.getConsolidado()
          GlobalMembers.preferences.set(Date(), forKey: GlobalMembers.dateKey)
        
          
       }
    }
    func getConsolidado()
    {
        SwiftSpinner.show("Cargando información...")
        invokeServ.requestConsolidadoDataWith { (response, error) in
            SwiftSpinner.hide()
            if(response != nil)
            {
                self.cons_array = response!
                
                
                DispatchQueue.main.async{
                    GlobalMembers.preferences.set(response!, forKey: GlobalMembers.consolidadoKey)
                    let didSave = GlobalMembers.preferences.synchronize()
                    self.tv_consolidado.refreshControl?.endRefreshing()
                    self.filldata()
                    
                }
                
                
                
            }
           
        }
    }
    func filldata()
    {
        var total_bancos = 0.0
        var total_clientes = 0.0
        var total_proveedores = 0.0
        for dic in self.cons_array {
            let company = dic as! NSDictionary
            
            let bank = company["bank_amount"] as! Double
            let clientes = company["clientes"] as! Double
            let proveedores = company["proveedores"] as! Double
            
            total_bancos = total_bancos + bank
            total_clientes = total_clientes + clientes
            total_proveedores = total_proveedores + proveedores
            
        }
        
        self.lbl_bancos_total.text = self.convertDoubleToCurrency(amount: total_bancos)
        self.lbl_clientes_total.text = self.convertDoubleToCurrency(amount: total_clientes)
        self.lbl_proveedores_total.text = self.convertDoubleToCurrency(amount: total_proveedores)
        self.tv_consolidado.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cons_array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "consolidado_cell", for: indexPath) as! ConsolidadoCell
        
        cell.backgroundColor = UIColor(red: 153/255.0, green: 135/255.0, blue: 157/255.0, alpha: 1.0).withAlphaComponent(0.1)
        cell.contentView.backgroundColor = UIColor(red: 153/255.0, green: 135/255.0, blue: 157/255.0, alpha: 1.0).withAlphaComponent(0.1)
        
        let company = cons_array[indexPath.row] as! NSDictionary
        
        let name = company["company"] as! String
        cell.lbl_name.text = name
        
        let bank_amount = company["bank_amount"] as! Double
        cell.lbl_bank.text = convertDoubleToCurrency(amount: bank_amount)
        
        let clientes = company["clientes"] as! Double
        cell.lbl_clients.text = convertDoubleToCurrency(amount: clientes)
        
        let proveedores = company["proveedores"] as! Double
        cell.lbl_suppliers.text = convertDoubleToCurrency(amount: proveedores)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    @IBAction func bacl_action(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
        
    }
    @IBAction func btn_filter(_ sender: Any) {
    }
    func updatelabeldate()
    {
        let date = GlobalMembers.preferences.object(forKey: GlobalMembers.dateConsolidadoKey) as! Date
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy HH:mm"
        print(df.string(from: date))
        lbl_date.text = df.string(from: date)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
