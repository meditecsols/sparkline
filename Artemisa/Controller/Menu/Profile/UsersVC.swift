//
//  UsersVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 24/08/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit
import SwiftSpinner

class UsersCell: UITableViewCell {
    
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_number: UILabel!
    
    
}

class UsersVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let invokeServ = InvokeService()
    let alertmanager = AlertManager()
    var profile_dict = NSDictionary()
    
    var users_array = NSArray()
    var position = Int()
    

    @IBOutlet weak var tv_users: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tv_users.isHidden = true
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.tv_users.backgroundView?.backgroundColor = UIColor.white
        self.tv_users.backgroundColor = UIColor.white
        getworkers()
    }
    
    func getworkers() {
        users_array = NSMutableArray()
        SwiftSpinner.show("Cargando...")
        invokeServ.requestWorkersDataWith { (response, error) in
                        
                        if response != nil
                        {
                            self.users_array = response!
                            
                            DispatchQueue.main.async
                            {
                                if(self.users_array.count > 0)
                                {
                                    self.tv_users.isHidden = false
                                }
                                SwiftSpinner.hide()
                                self.tv_users.reloadData()
                            }
                            
                        }
                        else
                        {
                            DispatchQueue.main.async
                            {
                                SwiftSpinner.hide()
                               
                            }
                            
                        }
                        
                        
                    }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users_array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "user_cell", for: indexPath) as! UsersCell
        cell.backgroundColor = UIColor.white
        cell.contentView.backgroundColor = UIColor.white
        let worker = users_array[indexPath.row] as! NSDictionary
        
        let id = worker.object(forKey: "id") as! Int
        let first_name = worker.object(forKey: "first_name") as! String
        let last_name = worker.object(forKey: "last_name") as! String
        
        cell.lbl_name.text = first_name + " " + last_name
        
        invokeServ.requestPermissionDataWith(id_worker: String(id)) { (response, error) in
            
            if(response!.count > 0)
            {
                let number = response?.count
                DispatchQueue.main.async
                {
                    cell.lbl_number.text =  String(number!)
                }
                
            }
            else
            {
                DispatchQueue.main.async
                {
                    cell.lbl_number.text = "0"
                }
            }
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        position = indexPath.row
        self.performSegue(withIdentifier: "segue_services", sender: nil)
        
    }
    
    @IBAction func back_action(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        let worker = users_array[position] as! NSDictionary
                       
        
        if segue.identifier == "segue_services" {
                   
            if let presented = segue.destination as? ServicesVC {
                
               
                let id = worker.object(forKey: "id") as! Int
                presented.director = self.profile_dict
                presented.id_user = id
                      
            }
            
        }
    }


}
