//
//  Config.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 07/05/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit
import Foundation

class Constants {

static let URL_PROD = "https://apolo.meditecsols.com/"
static let URL_SANDBOX = "https://sandbox.apolo.meditecsols.com/"
static let BASE_URL = URL_SANDBOX
    
static let URL_ARTEMISA_PROD = "http://api.mq-artemisa.com/"
static let BASE_URL_ARTEMISA = URL_ARTEMISA_PROD
}
