//
//  InvokeService.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 31/03/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit

class InvokeService: NSObject {
    
  
    let email = GlobalMembers.preferences.string(forKey: GlobalMembers.emailKey)
    
    //Login Reques
    
    func requestForLoginDataWith(_ params: NSMutableDictionary, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){
        //let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        let url = Constants.BASE_URL+"api/v0/login/"
        var request = URLRequest(url: URL(string: url)!)
               request.httpMethod = "POST"
               request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                 print(httpResponse!.statusCode)
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError
                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {
                            
                            completionHandler(dictonary, nil)
                        }
                        else
                        {
                            
                             completionHandler(dictonary, error)
                        }
                          
                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }
                else
                {
                     completionHandler(nil, error)
                }
                 
                  
                    //completionHandler(json, error!)
        
        
               })

               task.resume()
    }
    func requestForTokenDataWith(_ params: [NSMutableDictionary: NSMutableDictionary],token:String, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){
        //let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"o/token/")!)
               request.httpMethod = "POST"
               request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue("Basic " + token, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError
                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {
                           
                            completionHandler(dictonary,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }
                          
                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }
                else
                {
                    completionHandler(nil, error)
                }
                 
                  
                    //completionHandler(json, error!)
        
        
               })

               task.resume()
    }
    
    func requestForRecoverDataWith(_ params: [NSMutableDictionary: NSMutableDictionary], completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?,_ sttus: Int) -> Void){
        //let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"api/v0/login/recover/viaemail/")!)
               request.httpMethod = "POST"
               request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               //request.addValue(token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 201 {
                     // let errors: NSError?
                    //errors = error! as NSError
                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {
                           
                            completionHandler(dictonary,  error, httpResponse!.statusCode)
                        }
                        else
                        {
                             completionHandler(dictonary, error,httpResponse!.statusCode)
                        }
                          
                      } catch let error as NSError {
                         completionHandler(nil, error, httpResponse!.statusCode)
                      }
                }
                else
                {
                    completionHandler(nil, error,httpResponse!.statusCode)
                }
                 
                  
                    //completionHandler(json, error!)
        
        
               })

               task.resume()
    }
    
    func refreshTokenDataWith(completionHandler: @escaping (_ result: Bool?, _ error: Error?) -> Void){
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.basicTokenKey)
        let password = GlobalMembers.preferences.string(forKey: GlobalMembers.passwordKey)
        let email = GlobalMembers.preferences.string(forKey: GlobalMembers.emailKey)
        
        let params: NSMutableDictionary? = ["grant_type":"password" as Any, "username" : email!, "password" : password!]
        //let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"o/token/")!)
               request.httpMethod = "POST"
               request.httpBody = try? JSONSerialization.data(withJSONObject: params!, options: [])
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue("Basic " + token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError
                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                       
                        let accesstoken = dictonary!["access_token"] as! String
                        
                        let token = "Bearer " + accesstoken
                        
                        GlobalMembers.preferences.set(token, forKey: GlobalMembers.tokenKey)

                        
                        let didSave = GlobalMembers.preferences.synchronize()

                        if didSave {
                            
                            completionHandler(true, error)
                        }
                          
                      } catch let error as NSError {
                         completionHandler(false, error)
                      }
                }
                else
                {
                    completionHandler(false, error)
                }
                 
                  
                    //completionHandler(json, error!)
        
        
               })

               task.resume()
    }
    
    //Obtener Perfil
    
    func requestProfileDatabyWith(completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){

        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"api/v0/director/")!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue( token!, forHTTPHeaderField: "Authorization")
                
               let sessionConfig = URLSessionConfiguration.default
               sessionConfig.timeoutIntervalForResource = TimeInterval(2000)
               sessionConfig.timeoutIntervalForRequest = TimeInterval(2000)
            
               let session = URLSession(configuration: sessionConfig)
        
       
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray
                        if dictonary!.count == 1
                        {

                            completionHandler((dictonary?[0] as! NSDictionary) ,  error)
                        }
                        else
                        {
                            completionHandler(nil, error)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }
                else
                {
                   
                    //let returnData = String(data: data!, encoding: .utf8)
                    completionHandler(nil, error)
                    
                    
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    //Obtener Empresas
    
    func requestCompaniesDataWith(completionHandler: @escaping (_ result: NSArray?, _ error: Error?) -> Void){
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"api/v0/company/")!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray
                        if dictonary!.count == 1
                        {

                            completionHandler(dictonary ,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }
                else if httpResponse!.statusCode == 401
                {
                  
                    
                    completionHandler(nil, error)
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    //Obtener Report
    
    func requestReportTotalDatabyCompanyWith(company_id:String, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){

        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"api/v0/contpaq/" + company_id + "/get_report_total/")!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue( token!, forHTTPHeaderField: "Authorization")
                
               let sessionConfig = URLSessionConfiguration.default
               sessionConfig.timeoutIntervalForResource = TimeInterval(2000)
               sessionConfig.timeoutIntervalForRequest = TimeInterval(2000)
            
               let session = URLSession(configuration: sessionConfig)
        
       
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {

                            completionHandler(dictonary ,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }
                else
                {
                   
                    //let returnData = String(data: data!, encoding: .utf8)
                    completionHandler(nil, error)
                    
                    
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    //Obtener Report
    
    func requestReportByCompanyDatabyCompanyWith(company_id:String, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){

        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"api/v0/contpaq/" + company_id + "/get_report_by_company/")!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {

                            completionHandler(dictonary ,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }
                else
                {
                    completionHandler(nil, error!)
                    
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    //Obtener Conosolidado
    
    func requestConsolidadoDataWith(completionHandler: @escaping (_ result: NSArray?, _ error: Error?) -> Void){
        let date = Date()
        let monthString = date.month
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"api/v0/contpaq/get_consolidado/?month=" + monthString)! )
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( token!, forHTTPHeaderField: "Authorization")

        
               let sessionConfig = URLSessionConfiguration.default
               sessionConfig.timeoutIntervalForRequest = 300.0
               let session = URLSession(configuration: sessionConfig)
              // let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                         let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray
                        if dictonary!.count == 1
                        {

                            completionHandler(dictonary ,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }
                else
                {
                    let returnData = String(data: data!, encoding: .utf8)
                    completionHandler(nil, error!)
                    
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    //Obtener Workers
    
    func requestWorkersDataWith(completionHandler: @escaping (_ result: NSArray?, _ error: Error?) -> Void){

        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"api/v0/worker/get_workers/")! )
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( token!, forHTTPHeaderField: "Authorization")

        
               let sessionConfig = URLSessionConfiguration.default
               sessionConfig.timeoutIntervalForRequest = 300.0
               let session = URLSession(configuration: sessionConfig)
              // let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                         let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray
                        if dictonary!.count == 1
                        {

                            completionHandler(dictonary ,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }
                else
                {
                    let returnData = String(data: data!, encoding: .utf8)
                    completionHandler(nil, error!)
                    
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    //Obtener Permissions
    
    func requestPermissionDataWith(id_worker:String, completionHandler: @escaping (_ result: NSArray?, _ error: Error?) -> Void){

        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        
        let url_w = Constants.BASE_URL+"api/v0/worker_service/"+id_worker+"/by_worker/"
        var request = URLRequest(url: URL(string: url_w)! )
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( token!, forHTTPHeaderField: "Authorization")

        
               let sessionConfig = URLSessionConfiguration.default
               sessionConfig.timeoutIntervalForRequest = 300.0
               let session = URLSession(configuration: sessionConfig)
              // let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                         let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray
                        if dictonary!.count == 1
                        {

                            completionHandler(dictonary ,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }
                else
                {
                    //let returnData = String(data: data!, encoding: .utf8)
                    completionHandler(nil, error!)
                    
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    //Obtener Services
    
    func requestServicesDataWith(completionHandler: @escaping (_ result: NSArray?, _ error: Error?) -> Void){

        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        
        let url_w = Constants.BASE_URL+"api/v0/service/"
        
        var request = URLRequest(url: URL(string: url_w)! )
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( token!, forHTTPHeaderField: "Authorization")

        
               let sessionConfig = URLSessionConfiguration.default
               sessionConfig.timeoutIntervalForRequest = 300.0
               let session = URLSession(configuration: sessionConfig)
              // let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                         let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray
                        if dictonary!.count == 1
                        {

                            completionHandler(dictonary ,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }
                else
                {
                    //let returnData = String(data: data!, encoding: .utf8)
                    completionHandler(nil, error!)
                    
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    //Add Permission
    
    func requestaddPermissionDataWith(params:NSDictionary, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?, _ code:Int?) -> Void){

        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        
        let url_w = Constants.BASE_URL+"api/v0/worker_service/"
        
        var request = URLRequest(url: URL(string: url_w)! )
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( token!, forHTTPHeaderField: "Authorization")

        
               let sessionConfig = URLSessionConfiguration.default
               sessionConfig.timeoutIntervalForRequest = 300.0
               let session = URLSession(configuration: sessionConfig)
              // let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 201 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                         let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary

                            completionHandler(dictonary ,  error, httpResponse!.statusCode)
 

                      } catch let error as NSError {
                         completionHandler(nil, error, httpResponse!.statusCode)
                      }
                }
                else
                {
                    //let returnData = String(data: data!, encoding: .utf8)
                    completionHandler(nil, error!, httpResponse!.statusCode)
                    
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    //Remove Permission
    
    func requestRemovePermissionDataWith(id_permission:String,params:NSDictionary, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?, _ code:Int?) -> Void){

        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        
        let url_w = Constants.BASE_URL+"api/v0/worker_service/" + id_permission + "/"
        
        var request = URLRequest(url: URL(string: url_w)! )
        request.httpMethod = "PATCH"
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( token!, forHTTPHeaderField: "Authorization")

        
               let sessionConfig = URLSessionConfiguration.default
               sessionConfig.timeoutIntervalForRequest = 300.0
               let session = URLSession(configuration: sessionConfig)
              // let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                         let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary


                            completionHandler(dictonary ,  error, httpResponse!.statusCode)


                      } catch let error as NSError {
                         completionHandler(nil, error, httpResponse!.statusCode)
                      }
                }
                else
                {
                    let returnData = String(data: data!, encoding: .utf8)
                    completionHandler(nil, error!, httpResponse!.statusCode)
                    
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    
    
    // Bank Excel Request
    
    func requestBankataWith(company:String,completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?, _ code: Int?) -> Void){
        let date = Date()
        let monthString = date.month
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"api/v0/bank/"+company+"/get_bank_info/?month="+monthString)!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {

                            completionHandler(dictonary ,  error, httpResponse!.statusCode)
                        }
                        else
                        {
                             completionHandler(dictonary, error,httpResponse!.statusCode)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error,httpResponse!.statusCode)
                      }
                }
                else
                {
                  let returnData = String(data: data!, encoding: .utf8)
                    let dictonary =  NSDictionary()
                    completionHandler(dictonary, error,httpResponse!.statusCode)

                  
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    // CXP Request
    
    func requestCXPDataWith(completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"bi_cxp_sumary_by_company/"+email!)!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {

                            completionHandler(dictonary ,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    //CXC Clientes Request
    
    func requestCXCByCompanyDataWith(company_id:String,completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"bi_cxc_sumary_by_company_and_customer/" + company_id)!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {

                            completionHandler(dictonary ,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    //CXP Proveedores Request
    
    func requestCXPByCompanyDataWith(company_id:String,completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"bi_cxp_sumary_by_company_and_supplier/" + company_id)!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {

                            completionHandler(dictonary ,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    
    
    
    //Obtener Provisiones
    
    func requestProvisionDataWith(company_id:String, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"bi_cxp_sumary_payment_provision/" + company_id)!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {

                            completionHandler(dictonary ,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    //Obtener cuentas bancarias
    
    func requestBancosDataWith(company_id:String, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"bi_bks_sumary_currency_balance_by_company/" + company_id)!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {

                            completionHandler(dictonary ,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    //Obtener cuentas bancarias por empresas y bancos
    
    func requestBancosDatabyCompanyWith(company_id:String, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"bi_bks_sumary_currency_balance_by_company_and_bank/" + company_id)!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {

                            completionHandler(dictonary ,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    //Obtener Ordenes de compra
    
    func requestOrdennesDatabyCompanyWith(company_id:String, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Constants.BASE_URL+"bi_po_unauthorized_by_company_and_supplier/" + company_id)!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {

                            completionHandler(dictonary ,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }
                else
                {
                    completionHandler(nil, error!)
                    
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    
    //Obtener Ventas
       
       func requestVentasDatabyCompanyWith(company_id:String, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){
           let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
           var request = URLRequest(url: URL(string: Constants.BASE_URL+"bi_sales_sumary_by_company_and_customer/" + company_id)!)
                  request.httpMethod = "GET"
                  request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue( token!, forHTTPHeaderField: "Authorization")

                  let session = URLSession.shared
                  let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                      print(response!)
                   let httpResponse = response as? HTTPURLResponse
                   if httpResponse!.statusCode == 200 {
                        // let errors: NSError?
                       //errors = error! as NSError

                         //let returnData = String(data: data!, encoding: .utf8)
                         do {
                           let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                           if dictonary!.count == 1
                           {

                               completionHandler(dictonary ,  error)
                           }
                           else
                           {
                                completionHandler(dictonary, error)
                           }

                         } catch let error as NSError {
                            completionHandler(nil, error)
                         }
                   }
                   else
                   {
                       completionHandler(nil, error!)
                       
                   }


                       //completionHandler(json, error!)


                  })

                  task.resume()
       }
    
    
    
    ////Artemisa Apis
    
    //Login Request Artemisa
    
    func requestForLoginArtemisaDataWith(_ params: NSMutableDictionary, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){
        
        let url = Constants.BASE_URL_ARTEMISA+"login"
        var request = URLRequest(url: URL(string: url)!)
               request.httpMethod = "POST"
               request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                 print(httpResponse!.statusCode)
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError
                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {
                            
                            completionHandler(dictonary, nil)
                        }
                        else
                        {
                            
                             completionHandler(dictonary, error)
                        }
                          
                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }
                 
                  
        
               })

               task.resume()
    }
    
    
    //Obtener Empresas
    let email_artemisa = GlobalMembers.preferences.string(forKey: GlobalMembers.emailArtemisaKey)
    let token_artemisa = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenArtemisaKey)
    func requestCompaniesArtemisaDataWith(completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){

        var request = URLRequest(url: URL(string: Constants.BASE_URL_ARTEMISA+"bi_get_company_by_user/" + email!)!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( token_artemisa!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {

                            completionHandler(dictonary ,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }

                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    

}
